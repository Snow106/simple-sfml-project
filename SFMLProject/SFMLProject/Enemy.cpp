#include "Enemy.h"

Enemy::Enemy()
{
}

Enemy::~Enemy()
{
}

void Enemy::update(float deltaTime)
{
    m_sprite.move(m_speed * deltaTime, 0);
}

void Enemy::SetSpeed(float speed)
{
    m_speed = speed;
}

float Enemy::GetSpeed()
{
    return m_speed;
}

void Enemy::MoveDown()
{
    m_sprite.move(0, 40);
}
