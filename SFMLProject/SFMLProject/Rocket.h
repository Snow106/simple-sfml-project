#pragma once
#include "SFML-2.5.1\include\SFML\Graphics.hpp"
#include "Actor.h"

class Rocket : public Actor
{
	public:
		Rocket();
		virtual ~Rocket();

		virtual void update(float deltaTime) override;
};