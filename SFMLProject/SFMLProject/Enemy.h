#pragma once
#include "SFML-2.5.1/include/SFML/Graphics.hpp"
#include "Actor.h"

class Enemy : public Actor
{
	public:
		Enemy();
		virtual ~Enemy();

		void update(float deltaTime) override;

		void SetSpeed(float speed);
		float GetSpeed();
		void MoveDown();
};