#pragma once

#include "SFML-2.5.1\include\SFML\Graphics.hpp"
#include "Actor.h"

class Hero : public Actor
{
	public:
		Hero();
		virtual ~Hero();

		void update(float deltaTime) override;

		bool isMoving = false;
		int dir = 1;
};