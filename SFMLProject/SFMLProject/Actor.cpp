#include "Actor.h"

Actor::Actor()
{
}

Actor::~Actor()
{
}

void Actor::init(std::string textureName, sf::Vector2f position, float speed)
{
    m_position = position;

    m_texture.loadFromFile(textureName.c_str());

    m_sprite.setTexture(m_texture);
    m_sprite.setPosition(m_position);
    m_sprite.setOrigin(m_texture.getSize().x / 2, m_texture.getSize().y / 2);
    m_speed = speed;
}

void Actor::update(float deltaTime)
{
}

sf::Sprite Actor::getSprite()
{
    return m_sprite;
}
