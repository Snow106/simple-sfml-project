#include "SFML-2.5.1\include\SFML\Graphics.hpp"
#include <vector> 

#include "Actor.h"
#include "Hero.h" 
#include "Enemy.h"
#include "Rocket.h" 

sf::Vector2f viewSize(1024, 768);
sf::VideoMode vm(viewSize.x, viewSize.y);
sf::RenderWindow window(vm, "Hello SFMLGame", sf::Style::Default);

Hero hero;

std::vector<Enemy*> enemies;
std::vector<Rocket*> rockets;

float currentTime;
float prevTime = 0.0f;

int score = 0;
bool gameover = true;

sf::Font headingFont;
sf::Text headingText;
sf::Font scoreFont;
sf::Text scoreText;

void spawnEnemy(sf::Vector2f enemyPos);
void shoot();
bool checkCollision(sf::Sprite sprite1, sf::Sprite sprite2);
void reset();
void flipEnemies();


void init()
{

	hero.init("Assets/graphics/hero.png", sf::Vector2f(viewSize.x * 0.5f, viewSize.y - 40), 200);

	headingFont.loadFromFile("Assets/fonts/SnackerComic.ttf");
	headingText.setFont(headingFont);
	headingText.setString("Octopus Attack");
	headingText.setCharacterSize(84);
	headingText.setFillColor(sf::Color::White);

	scoreFont.loadFromFile("Assets/fonts/arial.ttf");

	scoreText.setFont(scoreFont);
	scoreText.setString("Score: 0");
	scoreText.setCharacterSize(45);
	scoreText.setFillColor(sf::Color::White);

	sf::FloatRect scorebounds = scoreText.getLocalBounds();
	scoreText.setOrigin(scorebounds.width / 2, scorebounds.height / 2);
	scoreText.setPosition(sf::Vector2f(viewSize.x * 0.5f, viewSize.y * 0.10f));

	sf::FloatRect headingbounds = headingText.getLocalBounds();
	headingText.setOrigin(headingbounds.width / 2, headingbounds.height / 2);
	headingText.setPosition(sf::Vector2f(viewSize.x * 0.5f, viewSize.y * 0.10f));

	srand((int)time(0));
}

void draw()
{
	window.draw(hero.getSprite());

	for (Enemy* enemy : enemies) 
	{
		window.draw(enemy->getSprite());
	}

	for (Rocket* rocket : rockets) 
	{
		window.draw(rocket->getSprite());
	}

	if (gameover) 
	{ 
         window.draw(headingText); 
    }
	else
	{
		window.draw(scoreText);
	}
}

void updateInput()
{
	sf::Event event;

	while (window.pollEvent(event))
	{
		if (event.type == sf::Event::KeyPressed)
		{
			if (event.key.code == sf::Keyboard::Right)
			{
				hero.dir = 1;
				hero.isMoving = true;
			}

			if (event.key.code == sf::Keyboard::Left) 
			{
				hero.dir = -1;
				hero.isMoving = true;
				if (gameover) 
				{
					gameover = false;
					reset();
				}
			}

			if (event.key.code == sf::Keyboard::Space)
			{
				shoot();
			}
		}

		if (event.type == sf::Event::KeyReleased)
		{
			if (event.key.code == sf::Keyboard::Right || event.key.code == sf::Keyboard::Left)
			{
				hero.isMoving = false;
			}
		}

		if (event.key.code == sf::Keyboard::Escape || event.type == sf::Event::Closed)
			window.close();
	}
}

void update(float deltaTime)
{
	hero.update(deltaTime);

	for (int i = 0; i < enemies.size(); i++) 
	{

		Enemy* enemy = enemies[i];

		enemy->update(deltaTime);

		if (enemy->getSprite().getPosition().x < 10 || enemy->getSprite().getPosition().x > 1014)
		{
			flipEnemies();
		}

		if (enemy->getSprite().getPosition().y > 750) 
		{

			enemies.erase(enemies.begin() + i);
			delete(enemy);
			gameover = true;
		}

		for (int i = 0; i < rockets.size(); i++)
		{

			Rocket* rocket = rockets[i];

			rocket->update(deltaTime);

			if (rocket->getSprite().getPosition().y < 0)
			{
				rockets.erase(rockets.begin() + i);
				delete(rocket);
			}
		}
	}

	for (int i = 0; i < rockets.size(); i++)
	{
		for (int j = 0; j < enemies.size(); j++)
		{
			Rocket* rocket = rockets[i];
			Enemy* enemy = enemies[j];

			if (checkCollision(rocket->getSprite(), enemy->getSprite()))
			{
				score++;
				std::string finalScore = "Score: " + std::to_string(score);
				scoreText.setString(finalScore);
				sf::FloatRect scorebounds = scoreText.getLocalBounds();
				scoreText.setOrigin(scorebounds.width / 2, scorebounds.height / 2);
				scoreText.setPosition(sf::Vector2f(viewSize.x * 0.5f, viewSize.y * 0.10f));

				rockets.erase(rockets.begin() + i);
				enemies.erase(enemies.begin() + j);

				delete(rocket);
				delete(enemy);

				printf(" rocket intersects enemy \n");
				return;
			}
		}
	}
}

int main()
{
	init();
	sf::Clock clock;

	while (window.isOpen())
	{
		updateInput();
		
		sf::Time deltaTime = clock.restart();
		if (!gameover)
			update(deltaTime.asSeconds());

		window.clear(sf::Color::Black);

		draw();

		window.display();
	}

	return 0;
}

void spawnEnemy(sf::Vector2f enemyPos)
{
	float speed = 200;

	Enemy* enemy = new Enemy();
	enemy->init("Assets/graphics/Alien.png", enemyPos, speed);

	enemies.push_back(enemy);
}

void shoot() 
{

	Rocket* rocket = new Rocket();

	rocket->init("Assets/graphics/Laser.png", hero.getSprite().getPosition(), -50.0f);

	rockets.push_back(rocket);
}

bool checkCollision(sf::Sprite sprite1, sf::Sprite sprite2) 
{

	sf::FloatRect shape1 = sprite1.getGlobalBounds();
	sf::FloatRect shape2 = sprite2.getGlobalBounds();

	if (shape1.intersects(shape2)) 
	{
		return true;
	}
	else 
	{
		return false;
	}

}

void reset() 
{

	score = 0;
	currentTime = 0.0f;
	prevTime = 0.0;

	for (Enemy* enemy : enemies) 
	{
		delete(enemy);
	}
	for (Rocket* rocket : rockets) 
	{
		delete(rocket);
	}

	enemies.clear();
	rockets.clear();

	for (int i = 0; i < 6; i++)
	{
		for (int j = 0; j < 3; j++)
		{
			spawnEnemy(sf::Vector2f(20 + 40 * i, 20 + 40 * j));
		}
	}
}

void flipEnemies()
{
	for (Enemy* enemy : enemies)
	{
		enemy->SetSpeed(enemy->GetSpeed() * -1);
		enemy->MoveDown();
	}
}