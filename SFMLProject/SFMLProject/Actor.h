#pragma once
#include "SFML-2.5.1/include/SFML/Graphics.hpp"

class Actor
{
public:
	Actor();
	virtual ~Actor();

	void init(std::string textureName, sf::Vector2f position, float speed);
	virtual void update(float deltaTime);

	virtual sf::Sprite getSprite();

protected:

	sf::Texture m_texture;
	sf::Sprite m_sprite;
	sf::Vector2f m_position;
	float m_speed;
};